#!/bin/env python3

import argparse
import yaml
import ipaddress
from datetime import date

from nmaptocsv import nmaptocsv

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("xml", help="XML File from which to merge")
    parser.add_argument("yaml", help="YAML File to merge into")
    parser.add_argument("-Pn", help="Assume the scan was performed with -Pn. Don't set last-seen unless an open port is observed", action="store_true")
    parser.add_argument("--date", help="custom value for last-seen. However, this won't influence if a entry is updated, it will always be updated, if new info is in the scan. Default: date of today")
    args = parser.parse_args()




    with open(args.yaml, "r") as yaml_file:
        network = yaml.load(yaml_file, Loader=yaml.SafeLoader)
    if network is None:
        network = {}


    count_new_ips = 0
    count_ips_before_import = len(network)

    scan_results = nmaptocsv.parse_xml(args.xml)

    for ip in scan_results:
        host_scan_result = scan_results[ip]
        host = {}
        name = host_scan_result.fqdn if len(host_scan_result.fqdn) > 0 else host_scan_result.rdns
        if len(name) > 0:
            host["name"] = name
        host["ports"] = list(map((lambda x: {"port": x.number, "service": x.service}), host_scan_result.ports))
        host["last-seen"] = args.date if args.date is not None else str(date.today())

        if args.Pn and (name not in host and len(host["ports"]) == 0):
            # host wasn't seen, just marked up because of -Pn scan, ignoring
            continue

        if host_scan_result.ip_dottedquad not in network:
            network[host_scan_result.ip_dottedquad] = host
            count_new_ips += 1
        else:
            network[host_scan_result.ip_dottedquad].update(host)

        # remove ports from yaml if no open ports were seen
        if len(host["ports"]) == 0:
            network[host_scan_result.ip_dottedquad].pop("ports")

    network_sorted = {}
    ips_sorted = list(map((lambda x: str(x)), sorted(list(map((lambda x: ipaddress.ip_address(x)), network.keys())))))
    for ip in ips_sorted:
        network_sorted[ip] = network[ip]
    with open(args.yaml, "w") as yaml_file:
        yaml.dump(network_sorted, sort_keys=False, stream=yaml_file)
    print(f"saw {len(scan_results)} IPs in nmap scan")
    print(f"thereof {count_new_ips} new IPs")
    print(f"{count_ips_before_import-len(scan_results)} IPs from YAML where not seen in nmap scan")

if __name__ == "__main__":
    main()
